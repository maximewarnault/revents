import {CREATE_EVENT, DELETE_EVENT, FETCH_EVENTS, UPDATE_EVENT} from "./eventConstants";
import {createReducer} from "../../app/common/util/reducerUtil";

const initialState = [];

export const createEvent = (state, payload) => {
  // ...state => split array in separate objects
  // Object.assign({}, payload.event) => add a new event beside separate objects
  // [] => creates a new array  with the old objects + new object
  return [...state, Object.assign({}, payload.event)];
};

export const updateEvent = (state, payload) => {
  return [
    ...state.filter(event => event.id !== payload.event.id),
    Object.assign({}, payload.event)
  ];
};

export const deleteEvent = (state, payload) => {
  return [...state.filter(event => event.id !== payload.eventId)];
};

export const fetchEvents = (state, payload) => {
    return payload.events
};

export default createReducer(initialState, {
    [CREATE_EVENT]: createEvent,
    [UPDATE_EVENT]: updateEvent,
    [DELETE_EVENT]: deleteEvent,
    [FETCH_EVENTS]: fetchEvents
});
