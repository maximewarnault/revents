import firebase from 'firebase';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyB7Kc00SI9rFREShgLUAvg848qQLjNMT08",
    authDomain: "revents-1807.firebaseapp.com",
    databaseURL: "https://revents-1807.firebaseio.com",
    projectId: "revents-1807",
    storageBucket: "revents-1807.appspot.com",
    messagingSenderId: "722160035220"
}

firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore();
const settings = {
    timestampsInSnapshots: true
};
firestore.settings(settings);
export default firebase;